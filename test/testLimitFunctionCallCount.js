limitFunctionCallCount = require("../limitFunctionCallCount");

result = limitFunctionCallCount(3);
result();
result();
result();
result();
result();
/* 
cb invoked
cb invoked
cb invoked 
null
null
*/
