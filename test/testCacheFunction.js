const cacheTracker = require("../cacheFunction");

const result = cacheTracker();

console.log(result("facebook"));
console.log(result("instagram"));
console.log(result("facebook"));
console.log(result("outlook"));
/*
{ facebook: 'facebook cached data' }

{
  facebook: 'facebook cached data',
  instagram: 'instagram cached data'
}

{
  facebook: 'facebook cached data',
  instagram: 'instagram cached data'
}

null
*/

