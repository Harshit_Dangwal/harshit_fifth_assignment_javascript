counterFactory = require("../counterFactory");

const result = counterFactory(5);
console.log(result.decrement())
console.log(result.decrement())
console.log(result.decrement())
console.log(result.decrement())
console.log(result.decrement())
console.log(result.decrement())
/* 
6
5
4
3  
2
1  

*/
console.log(result.increment())
console.log(result.increment())
console.log(result.increment())

/*
6
6
6
*/
