function limitFunctionCallCount(n) {
    let count=0
    return function cb() {
        if (count < n) {
            count++;
            return console.log("cb invoked");
        }else{
            return null;
        }
    }
}

//result = limitFunctionCallCount(3);





module.exports = limitFunctionCallCount;