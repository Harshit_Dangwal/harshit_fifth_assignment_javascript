function cacheTracker() {
    const cacheObj = {};
    let count = 0;
    return function cb(arg) {
        if (arg in cacheObj && count===0) {
            count++;
            return cacheObj;
        } else {
            if (count===0) {
                cacheObj[arg] = `${arg} cached data`;
                return cacheObj;
            }else{
                return null;
            }
        }
    }
}

//const result = cacheTracker();

module.exports = cacheTracker;