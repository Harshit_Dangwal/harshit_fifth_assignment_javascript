function counterFactory(counterV) {
    counterV++;
    return {
        increment: function () {
            return counterV;
        },
        decrement: function () {
            return counterV--;
        }
    }
}

//const result = counterFactory(1);

module.exports = counterFactory;